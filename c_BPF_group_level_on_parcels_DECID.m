% Computation of the second level statistics (across contacts of a parcel)
% for the paper "Intracerebral mechanisms explaining the impact of incidental
% feedback on mood state and risky choice"
%
% The SPM12 software is required to run this code and can be downloaded at the following address :
% https://www.fil.ion.ucl.ac.uk/spm/software/download/
%
% INPUTS :
%   - init = Structure containing the following fields :
%       - init.pat_names = Field of all patient identifiers of the form XXX_201X_XXXx
%       - init.bdd_path = Database path
%       - init.contrast = Contrasts to run (e.g. [4,7])
%           - 4  = Correlation real mood-ratings & baseline activity (choice onset locked)
%           - 7  = Correlation best TML & baseline activity (choice onset locked + all trials)
%           - 15 = Correlation baseline activity & residues of choice model (choice onset locked + all trials)
%       - init.parcel_names = Perform analysis only on electrodes that are in the selected parcels (e.g. {'PFCvm', 'aINS'})
%           - Name of all parcels: 'PFCvm','aINS_dors','aINS_vent','aINS','pINS','Thalamus','Caudate','Putamen','Pallidum','Hippocampus',...
%                                  'Amygdala','Accumbens','OFCv','OFCvm','OFCvl','Pfrdls','Pfrdli','VCl','VCcm','ICC','ITCm','VCs','Cu',...
%                                  'VCrm','ITCr','MTCc','MTCr','STCc','STCr','IPCv','IPCd','SPC','SPCm','PCm','Sv','Sdl','Sdm','PCC','MCC',...
%                                  'ACC','Mv','Mdl','Mdm','PMrv','PMdl','PMdm','PFcdl','PFcdm','PFrvl','PFrd','PFrm'
%       - init.freq_band (optional) = Frequency bands of the files to analyze (e.g. {[50,150]})
%       - init.smoothing (optional) = Smoothing of the files
%       - init.fsample (optional) = Sample frequency of files to analyse
%       - init.clust_cor (optional) = Find (1 = default) or not (0) threshold for cluster correction
%       - init.random_nb (optional) = Number of permutations for each parcel (only if init.clust_cor = 1)
%
% OUTPUT : An Excel file containing the statistics at the parcel level
%
%-------------------------------------------------------------------------%
% Romane Cecchi, 2022

function c_BPF_group_level_on_parcels_DECID(init)

clear;
close all;

%% Options

if ~isfield(init, 'freq_band')
    init.freq_band = {[50,150]}; % Choose frequency bands to analyse ({[4,8], [8,13], [13,33], [33,49], [50,150]})
end

if ~isfield(init, 'smoothing')
    init.smoothing = 250; % Choose smoothings to analyse
end

if ~isfield(init, 'fsample')
    init.fsample = 100; % Sample frequency of files to analyse
end

if ~isfield(init, 'clust_cor')
    init.clust_cor = 1; % Find threshold for cluster correction
end

if ~isfield(init, 'random_nb') && init.clust_cor == 1
    init.random_nb = 60000; % Number of permutation for each parcel
end

init.timewin   = [-4 0];     % Choose time window for statistical analyses ([-4 0])
init.exploretw = [-5.6 0.5]; % Choose time window for data exploration/visualisation ([-5.6 0.5])

%% Initialisation

init.path = fullfile(init.bdd_path, 'Patients');
init.anat_path = fullfile(init.bdd_path, 'Anatomie'); % Path where are .csv files
init.behav_path = fullfile(init.bdd_path, 'Comportement'); % Path were are mood model infos
init.first_level_path = fullfile(init.bdd_path, 'BPF_analyzes', 'Group_stat_BPF', sprintf('sf%d', init.fsample)); % Path where are the first level analyzes (BPF_stat.mat)
init.second_level_path = fullfile(init.bdd_path, 'BPF_analyzes', 'ROI_analyses_BPF', sprintf('sf%d', init.fsample)); % Path where new files will be saved

init.task = 'DECID';
init.alpha = 0.05;

%% Loop over parcels

for parcel = 1:length(init.parcel_names)
    
    close all
    clearvars -except init parcel parcels_data result
    
    [Tall, dots, ~, ~, store, chan_idx] = get_parcel_chan_BPF(init, parcel); % Find all the channels in a parcel through the subjects
    
    %% Save parcels infos
    
    for bpf = 1:length(init.freq_band) % Loop over filtered frequency bands
        
        f_range_name = sprintf('f%df%d', min(init.freq_band{bpf}), max(init.freq_band{bpf})); % Ex : f50f150
        
        for smooth = 1:length(init.smoothing) % Loop over smoothings
            
            smoothing = sprintf('sm%d',init.smoothing(smooth));
            
            for contrast = init.contrast % Loop over contrasts
                
                store_folder.(f_range_name).(smoothing) = fullfile(init.second_level_path, f_range_name, smoothing, sprintf('%s%s', store.(f_range_name).(smoothing)(contrast).contrastname));
                if ~exist(store_folder.(f_range_name).(smoothing), 'dir')
                    mkdir(store_folder.(f_range_name).(smoothing));
                end
                
                if ~isempty(dots.(f_range_name).(smoothing){1,contrast}) % S'il y a des contacts dans la parcelle
                    
                    timelist = store.(f_range_name).(smoothing)(contrast).filtered_timelist; % Time list of the contrast
                    
                    %% Compute cluster correction threshold
                    
                    if init.clust_cor == 1
                        
                        permbackup = fullfile(init.first_level_path, 'Permutation_stats', sprintf('tw_%.2f_%.2f', init.timewin), sprintf('contrast%d_%s_%s_%s.mat', contrast, init.parcel_names{parcel}, f_range_name, smoothing));
                        
                        if exist(permbackup, 'file')
                            load(permbackup, 'permstat');
                        else
                            permstat.roi = [];
                            
                            % Load permutation tests for all subjects
                            if ~isfield('init', 'perm_tests_all') % If the field "perm_tests_all" does not already exist
                                for pat_idx = 1:length(init.pat_names)
                                    
                                    perm_file = fullfile(init.first_level_path, "Permutation_tests", init.pat_names{pat_idx}, sprintf('contrast%d.mat', init.contrast)); % File containing permutations for 1 patient and 1 contrast
                                    load(perm_file, 'perm_tests'); % Load variables 'perm_tests'
                                    
                                    for freq = 1:length(init.freq_band) % Loop over filtered frequency bands
                                        freq_name = sprintf('f%df%d', min(init.freq_band{freq}), max(init.freq_band{freq})); % Ex : f50f150
                                        for smoo = 1:length(init.smoothing) % Loop over smoothings
                                            sm_name = sprintf('sm%d',init.smoothing(smoo));
                                            init.perm_tests_all(pat_idx).(freq_name).(sm_name) = perm_tests.(freq_name).(sm_name);
                                        end
                                    end
                                    clearvars perm_tests
                                end % End of the loop over subjects
                            end % End of the condition
                            
                        end
                        
                        if isempty(permstat.roi) % If field 'roi' is empty
                            contrastname = store.(f_range_name).(smoothing)(contrast).contrastname;
                            permstat = cluster_correction_threshold(init, chan_idx, init.perm_tests_all, f_range_name, smoothing, timelist, permstat);
                            if ~exist(fileparts(permbackup), 'dir')
                                mkdir(fileparts(permbackup));
                            end
                            save(permbackup, 'permstat', 'contrastname');
                        end
                    else
                        permstat.roi = [];
                    end
                    
                    %% Overall statistics
                    
                    Tallstat = Tall;
                    tw_idx = timelist >= init.timewin(1) & timelist <= init.timewin(2);
                    stat_timelist = timelist(tw_idx); % Filtered timelist for statistical analyses
                    
                    for prdct = 1:size(store.(f_range_name).(smoothing)(contrast).regressor, 2) % Loop over predictors
                        
                        % Find best cluster
                        avg_dots = mean(dots.(f_range_name).(smoothing){prdct, contrast}(tw_idx,:),2); % Average of regressor estimates (time course)
                        [h,~,~,stats] = ttest(dots.(f_range_name).(smoothing){prdct, contrast}(tw_idx,:)'); % Ttest on the time window we want to analyse
                        L = spm_bwlabel(h); %% Find clusters
                        
                        % Find the biggest cluster
                        stats_summary_tmp = NaN(max(L),4); % Initialization
                        for k = 1:max(L) % Analyse all clusters (numbered in L)
                            tmp = stat_timelist(L == k);
                            stats_summary_tmp(k,:) = [mean(avg_dots(L == k)), tmp(1), tmp(end), sum(stats.tstat(L == k))];
                            % (1) Moyenne des beta du cluster ; (2) Onset du cluster ; (3) Offset du cluster ; (4) Somme des t-values du cluster
                        end
                        
                        % Retrieve the data to put in the table
                        if isempty(stats_summary_tmp) % No cluster
                            best_clust = [0, NaN, NaN, NaN];
                            stats.tstat = NaN;
                            dots_err = NaN;
                        else
                            best_clust_idx = abs(stats_summary_tmp(:,4)) == max(abs(stats_summary_tmp(:,4))); % Index of the best cluster
                            best_clust = stats_summary_tmp(best_clust_idx,:);
                            
                            if ~isempty(permstat.roi) % Permutation correction
                                
                                abs_thresh = prctile(permstat.roi(:, 1, prdct), 100-(init.alpha)*100);
                                
                                if abs(best_clust(4)) > abs_thresh % Best cluster significant
                                    stats.tstat = best_clust(4); % Somme des t-values du plus gros cluster
                                    dots_err = length(find(permstat.roi(:, 1, prdct) > abs(best_clust(4)))) / length(permstat.roi(:, 1, prdct));
                                else
                                    stats.tstat = NaN;
                                    dots_err = NaN;
                                end
                                
                            else % No correction
                                [~, dots_err, ~, stats] = ttest(mean(dots.(f_range_name).(smoothing){1,contrast}(timelist >= best_clust(2) & timelist <= best_clust(3),:),1)); % Ttest de la moyenne des t-values du cluster � travers les �lectrodes
                            end
                        end
                        
                        % Collect summary info for this ROI and contrast
                        init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).parcel_name = init.parcel_names{parcel};
                        init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).nb_contacts = length(Tallstat.channel);
                        init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).nb_patients = length(unique(Tallstat.subname));
                        init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).best_cluster = [best_clust(2),best_clust(3)];
                        init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).mean_regression_coef = best_clust(1);
                        init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).tvalue = stats.tstat;
                        init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).pvalue = dots_err;
                        init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).nb_significant_patients = length(unique(Tallstat.subname(Tallstat.significance == 1)));
                        init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).significant_contact_prop = mean(Tallstat.significance);
                        init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).nb_significant_contact = sum(Tallstat.significance);
                        
                    end % End of the loop over predictors
                    
                else % There is no contact in the parcel
                    
                    init.tsum.(f_range_name).(smoothing){contrast}(parcel).parcel_name = init.parcel_names{parcel};
                    init.tsum.(f_range_name).(smoothing){contrast}(parcel).nb_contacts = 0;
                    
                end % End of the 'if there are contacts in the parcel'
                
            end % End of the loop across contrasts
        end % End of the loop over smoothings
    end % End of the loop over filtered frequency bands
    
end % Fin de la boucle � travers les parcelles

%% Save the table containing summary info for all ROIs

for bpf = 1:length(init.freq_band) % Loop over filtered frequency bands
    
    f_range_name = sprintf('f%df%d', min(init.freq_band{bpf}), max(init.freq_band{bpf})); % Ex : f50f150
    
    for smooth = 1:length(init.smoothing) % Loop over smoothings
        
        smoothing = sprintf('sm%d',init.smoothing(smooth));
        
        for contrast = init.contrast % Loop over contrasts
            
            if contrast ~= init.no_timeCourse % If the contrast contains a time course
                
                for prdct = 1:size(store.(f_range_name).(smoothing)(contrast).regressor, 2) % Loop over predictors
                    
                    Tsum = struct2table(init.tsum.(f_range_name).(smoothing){prdct, contrast});
                    
                    % Save the table containing infos for all ROIs
                    xlsfilesum = fullfile(store_folder.(f_range_name).(smoothing), sprintf('DECID_ROIs_%s_summary.xlsx',store.(f_range_name).(smoothing)(contrast).contrastname));
                    sheetname = sprintf('best cluster ; prdct = %d', prdct);
                    
                    if ~exist(xlsfilesum, 'file') % If the file doesn't exist yet
                        writetable(Tsum, xlsfilesum, 'Sheet', sheetname); % Write data
                    else
                        [~, sheetNames] = xlsfinfo(xlsfilesum); % Retrieve sheet names
                        
                        if ~ismember(sheetname,sheetNames) % If the sheet doesn't already exist
                            writetable(Tsum, xlsfilesum, 'Sheet', sheetname); % Write data
                        else % Clear sheet before writing
                            Excel = actxserver('Excel.Application'); % Open Excel as a COM Automation server
                            Workbook = Excel.Workbooks.Open(xlsfilesum); % Open Excel workbook
                            cellfun(@(x) Excel.ActiveWorkBook.Sheets.Item(x).Cells.Clear, {sheetname}); % Clear the content of the sheets
                            Workbook.Save; % Save
                            Excel.Workbook.Close; % Close
                            invoke(Excel, 'Quit'); % Quit
                            delete(Excel) % Delete Excel actxserver
                            writetable(Tsum, xlsfilesum, 'Sheet', sheetname); % Write data
                        end
                    end
                    
                end % End of the loop over predictors
            end % End of 'if the contrast contains time course'
        end % Enf of the loop across contrasts
    end % End of the loop over smoothings
end % End of the loop over filtered frequency bands

close all

end % End of the function
