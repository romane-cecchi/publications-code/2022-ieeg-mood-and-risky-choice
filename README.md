# 2022 - iEEG mood and risky choice

This repository contains the Matlab code for:

Cecchi, R., Vinckier, F., Hammer, J., Marusic, P., Nica, A., Rheims, S., Trebuchon, A., Barbeau, E., Denuelle, M., Maillard, L., Minotti, L., Kahane, P., Pessiglione, M., & Bastin, J. (2022). **Intracerebral mechanisms explaining the impact of incidental feedback on mood state and risky choice**. eLife. https://doi.org/10.7554/eLife.72440

## The codes

- `a_BPF_computations_DECID.m`: Extract the different frequency envelopes, and in particular the broadband gamma activity (BGA), from the raw intracranial data
- `b_BPF_iEEG_first_level_DECID.m`: Perform the regression analyses at recording site level
- `c_BPF_group_level_on_parcels_DECID.m`: Compute the second level statistics (across all recording sites of a ROI)

The SPM12 software is required to run this code and can be downloaded at the following address : https://www.fil.ion.ucl.ac.uk/spm/software/download/

All other custom codes used are located in the "subfunctions" folder.

## Data

The human data source (iEEG) needed to run these codes is currently restricted from open sharing by university policy, but may be shared with qualified investigators under appropriate institutional protections upon request.

