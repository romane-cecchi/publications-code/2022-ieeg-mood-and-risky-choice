% Regression analyses at the contact-level for the paper "Intracerebral
% mechanisms explaining the impact of incidental feedback on mood state and
% risky choice"
%
% The SPM12 software is required to run this code and can be downloaded at the following address :
% https://www.fil.ion.ucl.ac.uk/spm/software/download/
%
% INPUTS :
%   - init = Structure containing the following fields :
%       - init.pat_names = Field of patient identifiers of the form XXX_201X_XXXx
%       - init.bdd_path = Database path
%       - init.cont = Contrasts to run (e.g. [4,7])
%           - 4  = Correlation real mood-ratings & baseline activity (choice onset locked)
%           - 7  = Correlation best TML & baseline activity (choice onset locked + all trials)
%           - 15 = Correlation baseline activity & residues of choice model (choice onset locked + all trials)
%       - init.parcels = Perform analysis only on electrodes that are in the selected parcels (e.g. {'PFCvm', 'aINS'})
%           - 'all' = all parcels
%           - Name of all parcels: 'PFCvm','aINS_dors','aINS_vent','aINS','pINS','Thalamus','Caudate','Putamen','Pallidum','Hippocampus',...
%                                  'Amygdala','Accumbens','OFCv','OFCvm','OFCvl','Pfrdls','Pfrdli','VCl','VCcm','ICC','ITCm','VCs','Cu',...
%                                  'VCrm','ITCr','MTCc','MTCr','STCc','STCr','IPCv','IPCd','SPC','SPCm','PCm','Sv','Sdl','Sdm','PCC','MCC',...
%                                  'ACC','Mv','Mdl','Mdm','PMrv','PMdl','PMdm','PFcdl','PFcdm','PFrvl','PFrd','PFrm'
%       - init.freq_band (optional) = Frequency bands of the files to analyze (e.g. {[50,150]})
%       - init.smoothing (optional) = Smoothing of the files
%       - init.fsample (optional) = Sample frequency of files to analyse
%       - init.perm_test (optional) = Perform (1) or not (0) permutation corrections
%       - init.perm_nb (optional) = Number of permutations (only if init.perm_test = 1)
%
% OUTPUT : One file per subject with contact-level T-values + statistics
%
%-------------------------------------------------------------------------%
% Romane Cecchi, 2022

function b_BPF_iEEG_first_level_DECID(init)

clear;
close all;

%% Options

if ~isfield(init, 'freq_band')
    init.freq_band = {[50,150]}; % Choose frequency bands to analyse ({[4,8], [8,13], [13,33], [33,49], [50,150]})
end

if ~isfield(init, 'smoothing')
    init.smoothing = 250; % Choose smoothing to analyse
end

if ~isfield(init, 'fsample')
    init.fsample = 100; % Sample frequency of files to analyse
end

if ~isfield(init, 'perm_test')
    init.perm_test = 1; % Perform permutation tests (1 = yes / 0 = no)
end

if  ~isfield(init, 'perm_nb') && init.perm_test == 1
    init.perm_nb = 300; % Number of permutations
end

%% Initialisation

init.path = fullfile(init.bdd_path, 'Patients');
init.backuproot = fullfile(init.bdd_path, 'BPF_analyzes');
init.behav_path = fullfile(init.bdd_path, 'Comportement');

init.nb_sess = 4; % Nombre de sessions possible

%-------------------------------------------------------------------------%
% Because the human data source used in this manuscript is limited by university
% policy, mood and choice data files can only be provided upon request

% Load theoretical mood data
mood_data_fullname = fullfile(init.behav_path,'all_mood_data.mat');
load(mood_data_fullname, 'mood_data'); % Load 'mood_data' structure

% Load choice model data
choice_data_fullname = fullfile(init.behav_path,'all_choice_data.mat');
load(choice_data_fullname, 'choice_data'); % Load 'choice_data' structure

%% Boucle � travers les sujets

for pat_idx = 1:length(init.pat_names)
    tic;
    
    clearvars -except init mood_data choice_data pat_idx
    subjName = init.pat_names{pat_idx};
    
    removebadchannels = 1;
    
    %% Path where the data are
    
    subjtask = sprintf('%s_%s', subjName, 'DECID');
    patfile = fullfile(init.path, subjName, subjtask);
    
    %% Load EPI_BPF_stat.mat of the subject if exist
    
    filename = sprintf('%s_%s_BPF_stat.mat', subjName, 'DECID');
    filebackup = fullfile(init.backuproot, 'Group_stat_BPF', sprintf('sf%d', init.fsample), filename);
    if exist(filebackup, 'file')
        load(filebackup, 'store');
    end
    
    if ~exist(fileparts(filebackup), 'dir') % Si le dossier n'existe pas on le cr��
        mkdir(fileparts(filebackup));
    end
    
    %% STEP 1: LOAD BEHAVIORAL DATA
    
    % Load data matrix
    subjdata = [];
    for sess = 1:init.nb_sess % Loop over sessions
        
        data_filename = fullfile(patfile, 'RAW', 'Comportement', sprintf('%s%d_data.mat', subjtask, sess));
        
        if exist(data_filename,'file') % Si le fichier de la session existe
            load(data_filename, 'data'); % Charger la matrice data de la session
            subjdata = [subjdata ; data]; % Matrice contenant les donn�es de toutes les sessions pour un sujet
        end
        
    end % Fin de la boucle � travers les sessions
    
    %% Loop over frequency bands and smoothings
    
    for bpf = 1:length(init.freq_band) % Loop over filtered frequency bands
        
        f_range = init.freq_band{bpf}; % Frequency range
        f_range_name = sprintf('f%df%d', min(f_range), max(f_range));
        
        for smooth = 1:length(init.smoothing) % Loop over smoothings
            
            smoothing = sprintf('sm%d',init.smoothing(smooth));
            
            %% STEP 2: LOAD EEG DATA
            
            bpffilename = fullfile(patfile, 'Analyses', 'BPF', sprintf('%s_%s_f%df%d_sf%d_sm%d', subjName, 'DECID', min(f_range), max(f_range), init.fsample, init.smoothing(smooth)));
            D = spm_eeg_load(bpffilename);
            
            % Load data
            alldata = permute(D(:,:,:), [3 2 1]); % Ntrials x Ntimes x Nchan
            
            m_cond = str2double(D.conditions'); % Events order used to create the TF matrix
            timelist = D.time;
            chanlabels = D.chanlabels;
            
            % We will use 'alldata' as the data matrix to perform single channel statistics
            
            %% STEP 3: APPLY SOME OPTIONS
            
            if removebadchannels == 1
                badchan = [];
                sess = unique(subjdata(:,2));
                for sbc = 1:length(sess) % Loop over sessions
                    bcfilename = fullfile(patfile, 'Analyses', 'BadChannel', sprintf('sess%d', sess(sbc)), sprintf('bc_sess%d.mat', sess(sbc)));
                    BC = spm_eeg_load(bcfilename);
                    badchan = [badchan BC.badchannels];
                end % End of the loop over sessions
                badchan = unique(badchan); % Delete the channel if it's bad in at least one session
                alldata(:,:,badchan) = [];
                chanlabels(badchan) = [];
            end
            
            %% Find channels in specified parcel
            
            if ismember(init.parcels, 'all')
                ROI_chan_idx = 1:length(chanlabels);
            else
                ROI_chan_idx = [];
                
                % Get parcel infos for each sEEG channel from .CSV file
                csvfile = fullfile(fullfile(init.bdd_path, 'Anatomie'), init.pat_names{pat_idx}, sprintf('%s.csv', init.pat_names{pat_idx}));
                csv = csv2anat2(csvfile, chanlabels);
                
                for p = 1:length(init.parcels)
                    % Specify all the contraints that will be used to define a given ROI
                    constraints = anat_constraints(init.parcels{p});
                    idx = test_chan_parcel(csv, constraints);
                    ROI_chan_idx = [ROI_chan_idx ; idx];
                end
                ROI_chan_idx = unique(ROI_chan_idx);
            end
            
            %% STEP 4: CONTRAST BETWEEN CONDITIONS
            close all;
            
            for tidx = 1:length(init.cont) % Loop over contrasts
                
                if exist('store','var') && isfield(store,f_range_name) && isfield(store.(f_range_name),smoothing) && init.cont(tidx) <= length(store.(f_range_name).(smoothing)) && ~isempty(store.(f_range_name).(smoothing)(init.cont(tidx)).contrastname) % If the contrast already exist
                    store.(f_range_name).(smoothing)(init.cont(tidx)) = structfun(@(x) [], store.(f_range_name).(smoothing)(init.cont(tidx)), 'UniformOutput', false); % Empty it
                end
                
                % Load Permutation_tests\XXX_20XX_NNNp\contrastX.mat if exist
                if init.perm_test == 1 % Permutation tests
                    permbackup = fullfile(init.backuproot, 'Group_stat_BPF', sprintf('sf%d', init.fsample), 'Permutation_tests', subjName, sprintf('contrast%d.mat', init.cont(tidx)));
                    if exist(permbackup, 'file') % Si le contrast existe d�j�
                        load(permbackup, 'perm_tests')
                        if isfield(perm_tests,f_range_name) && isfield(perm_tests.(f_range_name),smoothing) % if perm_test for the freq and smoothing considered already exist, empty it
                            if pat_idx == 1 && size(perm_tests.(f_range_name).(smoothing).dots{1},3) > init.perm_nb
                                answer = questdlg(sprintf('Attention : Le nombre de permutation est inf�rieur � celui existant.\nLancer quand m�me l''analyse ?'), 'Warning', 'Oui', 'Non', 'Non');
                                if strcmp(answer, 'Non')
                                    return
                                end
                            end
                            perm_tests.(f_range_name).(smoothing).dots = {};
                        end
                    end
                end
                
                % Switch between contrasts
                switch init.cont(tidx)
                    
                    case 4 % Correlation real mood-ratings & baseline activity (choice onset locked + mood rating trials)
                        
                        contrastname = 'mood-rating-choice-onset-locked';
                        Aidx = find(ismember(m_cond,100)) + 1; % Onset du choix aux essais o� il y a eu un mood rating
                        store.(f_range_name).(smoothing)(init.cont(tidx)).regressor = zscore(subjdata(~isnan(subjdata(:,13)),13)); % Valeurs des mood ratings z-scor�es par SUJET
                        
                        % Fen�tre que l'on veut regarder autour de notre �v�nement
                        tmin = -6;
                        tmax = 1;
                        
                        label = 'time (s) relative to choice onset';
                        
                    case 7 % Correlation best TML & baseline activity (choice onset locked + all trials)
                        
                        contrastname = 'best-TML-choice-onset-locked-all-trials';
                        Aidx = find(ismember(m_cond,70:73)); % Onset du choix
                        store.(f_range_name).(smoothing)(init.cont(tidx)).regressor = zscore(mood_data.(subjName).TML_best'); % Best TML all trials (specific to the subject)
                        
                        % Fen�tre que l'on veut regarder autour de notre �v�nement
                        tmin = -6;
                        tmax = 1;
                        
                        label = 'time (s) relative to choice onset';
                        
                    case 15 % Correlation residues of choice model & baseline activity (choice onset locked + all trials)
                        
                        contrastname = 'residues-choice-onset-locked-all-trials';
                        obs_data = zscore(choice_data.inversionResult.model(1).(subjName).out.suffStat.dy'); % R�sidus du mod�le de choix z-scor�s
                        Aidx = find(ismember(m_cond,70:73)); % Onset du choix
                        
                        % Fen�tre que l'on veut regarder autour de notre �v�nement
                        tmin = -6;
                        tmax = 1;
                        
                        label = 'time (s) relative to choice onset';
                        
                end
                
                store.(f_range_name).(smoothing)(init.cont(tidx)).contrastname = contrastname;
                if exist('label','var')
                    store.(f_range_name).(smoothing)(init.cont(tidx)).xlabel = label;
                end
                
                filterin = timelist >= tmin & timelist <= tmax;
                
                for chan = 1:length(chanlabels) % Boucle � travers les channels (calcul stats + figures)
                    if ismember(chan, ROI_chan_idx) % If the channel is one of the selected parcels
                        
                        switch init.cont(tidx) % Switch between contrasts
                            
                            case {4,7}
                                
                                z_alldata = zscore(alldata(Aidx,:,:));
                                obs_data = z_alldata(:,filterin,chan); % Trial x Time matrix
                                
                                % Exceptions for the two patients whose end
                                % of a file is missing (function provided only on request)
                                store.(f_range_name).(smoothing)(init.cont(tidx)).regressor = exception_patients(subjName, store.(f_range_name).(smoothing)(init.cont(tidx)).regressor, obs_data);
                                
                                dots = NaN(size(obs_data,2),size(store.(f_range_name).(smoothing)(init.cont(tidx)).regressor,2));
                                t = dots;
                                dots_err = dots;
                                
                                % Loop over time points to get regression weights and stats
                                for timi = 1:size(obs_data,2) % R�gression � chaque temps
                                    [b,~,stats] = glmfit(store.(f_range_name).(smoothing)(init.cont(tidx)).regressor, obs_data(:,timi));
                                    dots(timi,:) = b(2:end)'; % Regression coefficient (= slope) of the regression line
                                    t(timi,:) = stats.t(2:end)';
                                    dots_err(timi,:) = stats.p(2:end)';
                                end
                                
                                % Permuation tests
                                if init.perm_test == 1
                                    dots_perm = NaN(size(obs_data,2),size(store.(f_range_name).(smoothing)(init.cont(tidx)).regressor,2),init.perm_nb);
                                    t_perm = dots_perm;
                                    dots_err_perm = dots_perm;
                                    for perm = 1:init.perm_nb
                                        perm_reg = Shuffle(store.(f_range_name).(smoothing)(init.cont(tidx)).regressor, 2); % Shuffle predictors
                                        for timi = 1:size(obs_data,2) % R�gression � chaque temps
                                            [b_perm,~,stats_perm] = glmfit(perm_reg, obs_data(:,timi));
                                            dots_perm(timi,:,perm) = b_perm(2:end)'; % Regression coefficient (= slope) of the regression line
                                            t_perm(timi,:,perm) = stats_perm.t(2:end)';
                                            dots_err_perm(timi,:,perm) = stats_perm.p(2:end)';
                                        end
                                    end
                                end
                                
                            case {15}
                                
                                z_alldata = zscore(alldata(Aidx,:,:));
                                predictors(:,:,1) = z_alldata(:,filterin,chan);
                                
                                store.(f_range_name).(smoothing)(init.cont(tidx)).regressor = permute(predictors, [1, 3, 2]); % Trials x Regressor x Time matrix
                                
                                % Exceptions for the two patients whose end of a file is missing
                                obs_data = exception_patients(subjName, obs_data, store.(f_range_name).(smoothing)(init.cont(tidx)).regressor);
                                
                                dots = NaN(size(store.(f_range_name).(smoothing)(init.cont(tidx)).regressor,3), size(store.(f_range_name).(smoothing)(init.cont(tidx)).regressor,2));
                                t = dots;
                                dots_err = dots;
                                
                                % Loop over time points to get regression weights and stats
                                for timi = 1:size(store.(f_range_name).(smoothing)(init.cont(tidx)).regressor,3) % R�gression � chaque temps
                                    [b,~,stats] = glmfit(store.(f_range_name).(smoothing)(init.cont(tidx)).regressor(:,:,timi), obs_data);
                                    dots(timi,:) = b(2:end)'; % Regression coefficient (= slope) of the regression line
                                    t(timi,:) = stats.t(2:end)';
                                    dots_err(timi,:) = stats.p(2:end)';
                                end
                                
                                % Permuation tests
                                if init.perm_test == 1
                                    dots_perm = NaN(size(store.(f_range_name).(smoothing)(init.cont(tidx)).regressor,3), size(store.(f_range_name).(smoothing)(init.cont(tidx)).regressor,2), init.perm_nb);
                                    t_perm = dots_perm;
                                    dots_err_perm = dots_perm;
                                    for perm = 1:init.perm_nb
                                        perm_reg = Shuffle(obs_data); % Shuffle response
                                        for timi = 1:size(store.(f_range_name).(smoothing)(init.cont(tidx)).regressor,3) % R�gression � chaque temps
                                            [b_perm,~,stats_perm] = glmfit(store.(f_range_name).(smoothing)(init.cont(tidx)).regressor(:,:,timi), perm_reg);
                                            dots_perm(timi,:,perm) = b_perm(2:end)'; % Regression coefficient (= slope) of the regression line
                                            t_perm(timi,:,perm) = stats_perm.t(2:end)';
                                            dots_err_perm(timi,:,perm) = stats_perm.p(2:end)';
                                        end
                                    end
                                end
                                
                        end % End of the switch between contrasts
                        
                        store.(f_range_name).(smoothing)(init.cont(tidx)).dots{chan} = dots; % We store regression coefficients for each time point
                        store.(f_range_name).(smoothing)(init.cont(tidx)).tstat{chan} = t; % On stock les t-values
                        store.(f_range_name).(smoothing)(init.cont(tidx)).pval{chan} = dots_err; % On stock les p-values
                        
                        if init.perm_test == 1 % Permutation tests
                            perm_tests.(f_range_name).(smoothing).dots{chan} = dots_perm; % Time x Nb regressor x Nb permutation
                            perm_tests.(f_range_name).(smoothing).tstat{chan} = t_perm;
                            perm_tests.(f_range_name).(smoothing).pval{chan} = dots_err_perm;
                        end
                        
                    else % The channel is not within a parcel
                        
                        store.(f_range_name).(smoothing)(init.cont(tidx)).dots{chan} = NaN; % We store regression coefficients for each time point
                        store.(f_range_name).(smoothing)(init.cont(tidx)).tstat{chan} = NaN; % On stock les t-values
                        store.(f_range_name).(smoothing)(init.cont(tidx)).pval{chan} = NaN; % On stock les p-values
                        
                        if init.perm_test == 1 % Permutation tests
                            perm_tests.(f_range_name).(smoothing).dots{chan} = NaN; % Time x Nb regressor x Nb permutation
                            perm_tests.(f_range_name).(smoothing).tstat{chan} = NaN;
                            perm_tests.(f_range_name).(smoothing).pval{chan} = NaN;
                        end
                        
                    end % Fin de la contrainte "si la channel est dans une parcelle"
                end % Fin de la boucle � travers les channels
                
                store.(f_range_name).(smoothing)(init.cont(tidx)).filtered_timelist = timelist(filterin);
                
                % Permutation tests
                if init.perm_test == 1
                    perm_tests.(f_range_name).(smoothing).contrastname = contrastname;
                    
                    if ~exist(fileparts(permbackup), 'dir') % Si le dossier n'existe pas on le cr��
                        mkdir(fileparts(permbackup));
                    end
                    
                    try
                        save(permbackup, 'subjName', 'perm_tests');
                    catch % If the file is > 2GB
                        save(permbackup, 'subjName', 'perm_tests', '-v7.3');
                    end
                end
                
            end % Fin de la boucle � travers les contrastes
        end % Fin de la boucle � travers les smoothings
    end % Fin de la boucle � travers les bandes de fr�quence
    
    % Fill hdr structure
    hdr.dimord = 'trial_time_chan';
    hdr.dimsiz = size(alldata);
    hdr.conditions = m_cond;
    hdr.timelist = timelist;
    hdr.chanlist = chanlabels';
    
    if ~exist(fullfile(init.backuproot, 'Group_stat_BPF'), 'dir')
        mkdir(fullfile(init.backuproot, 'Group_stat_BPF'));
    end
    save(filebackup, 'subjName', 'hdr', 'store');
    
    fprintf('Elapsed time for patient #%d (%s) is %.2f seconds\n', pat_idx, subjName, toc);
    
end % Fin de la boucle � travers les sujets

return % END

end % End of the function
