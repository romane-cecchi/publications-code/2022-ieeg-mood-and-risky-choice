% Performs band-pass filtering with an spm file as input
%
% INPUTS :
%   - D = SPM structure
%   - f_range = Frequency range to filter (e.g. 50:10:150)
%   - fileout = Full path of the output file WITHOUT extension or filtering infos
%   - smoothing = (optional) Smoothing windows in ms (e.g. [0 250])
%                 If not defined, smoothing = 0
%
% OUTPUT :
%   - filename = Names of the files generated
%--------------------------------------------------------------------------
% Romane Cecchi, 2018

function filename = spm2env(D,f_range,fileout,smoothing)

if nargin < 4 % Smoothing not defined
    smoothing = 0;
end

smoothing_sam = round(D.fsample*smoothing/1000);

for sm = 1:length(smoothing)
    m_allsc.(sprintf('s%d', smoothing(sm))) = zeros(D.nsamples,D.nchannels);
end

for bip = 1:length(D.chanlabels) % Boucle � travers les bipoles
    
    fprintf('filtering data from bipole %d out of %d\n', bip, length(D.chanlabels));
    
    m_datae = zeros(D.nsamples,1);
    
    for s_f = 1:length(f_range)-1 % Boucle � travers les fr�quences
        
        s_fmin = f_range(s_f);
        s_fmax = f_range(s_f + 1);
        m_dataf = bpfilter(D(bip,:,:)',s_fmin,s_fmax,D.fsample); % Filtre
        m_dataf = env_hilb(m_dataf); % Extraction de l'enveloppe (puissance)
        
        % Remove edge effects
        m_datafm = mean(m_dataf(round(D.nsamples/4):3*round(D.nsamples/4),:));
        v_fff = find(m_datafm == 0);
        if ~isempty(v_fff)
            m_datafm(v_fff) = 1; % Replace 0 by 1
        end
        m_datafm = repmat(m_datafm,size(m_dataf,1),1);
        
        m_dataf = 100*m_dataf./m_datafm; % Divide by mean for every frequency
        % m_dataf is now expressed in % (100 % = average power in the band)
        
        m_datae = m_datae + m_dataf;
        
    end % Fin de la boucle � travers les fr�quences
    
    m_datae = 10*m_datae / (length(f_range)-1); % Averaging of consecutive frequency bands
    % All values are now expressed in % (per thousands - pour mille) of mean value % DB
    
    % Smoothing
    for sm = 1:length(smoothing) % Boucle � travers les smoothings
        
        if smoothing_sam(sm) == 0 % No smoothing
            m_datac = m_datae;
        else
            m_datac = conv2(m_datae', ones(1,smoothing_sam(sm))/(smoothing_sam(sm)), 'same');
            m_datac = m_datac'; % Now sample x channel, just like m_datae
        end
        m_allsc.(sprintf('s%d', smoothing(sm)))(:,bip) = m_datac;
        
    end
    
end % Fin de la boucle � travers les bipoles

% Write SPM files
disp('writing data');

for sm = 1:length(smoothing) % Boucle � travers les smoothings
    
    % Replace NaN by 0
    m_allsc.(sprintf('s%d', smoothing(sm)))(isnan(m_allsc.(sprintf('s%d', smoothing(sm))))) = 0;
    
    % Write SPM files
    filename{sm} = sprintf('%s_f%df%d_sf%d_sm%d', fileout, min(f_range), max(f_range), D.fsample, smoothing(sm));
    Dnew = copy(D,filename{sm});
    Dnew(:,:,1) = m_allsc.(sprintf('s%d', smoothing(sm)))(:,:)';
    save(Dnew);
    
end

end