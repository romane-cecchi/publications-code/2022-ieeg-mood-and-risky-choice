% Get all channels of a specified parcel and the associated informations
%
% INPUTS :
%   - init : structure
%       - init.parcel_names = cell array with parcels name
%       - init.pat_names = cell array with patients name
%       - init.task = name of the task analysed (e.g. 'DEDID')
%       - init.first_level_path = path where are the first level analyzes (BPF_stat.mat)
%       - init.anat_path = path where are .csv files
%       - init.freq_band = frequency bands to analyse
%       - init.smoothing = smoothings to analyse
%   - parcel : index of parcel in init.parcel_names
%
% OUTPUTS :
%   - Tall = table with informations about the selected channels
%   - dots = structure for each contrast of all regression coefficients for each channels (timelist x nb of contact in the ROI)
%   - pval = structure for each contrast of all p-value for each channels (timelist x nb of contact in the ROI)
%   - tstat = structure for each contrast of all t-value for each channels (timelist x nb of contact in the ROI)
%   - store = structure containing infos about contrasts
%   - chan_idx (optional) = cell containing electrodes number in the parcel for each patient
%
%-------------------------------------------------------------------------%
% Romane Cecchi, 2018

function [Tall, dots, pval, tstat, store, chan_idx] = get_parcel_chan_BPF(init, parcel)

% Specify all the contraints that will be used to define a given ROI
constraints = anat_constraints(init.parcel_names{parcel});

% Initialize variables
Tall = [];

%% Loop over subjects

for pat_idx = 1:length(init.pat_names)
    
    subid = [init.pat_names{pat_idx} '_' init.task];
    
    %% STEP 1 : Get chanlabels from TF file
    
    eegfilename = sprintf('%s_BPF_stat.mat', subid);
    load(fullfile(init.first_level_path, eegfilename), 'hdr', 'store'); % Load variables 'hdr' and 'store'
    chanlabels = hdr.chanlist;
    
    %% Initialize dots and pstat (as structures identical to "store")
    
    if ~exist('dots','var')
        fieldn = fieldnames(store);
        for i = 1:size(fieldn,1)
            fieldsm = fieldnames(store.(fieldn{i}));
            for j = 1:size(fieldsm,1)
                dots.(fieldn{i}).(fieldsm{j}) = cell(max(cellfun('size', {store.(fieldn{i}).(fieldsm{j}).regressor},2)),length({store.(fieldn{i}).(fieldsm{j}).contrastname}));
            end
        end
        tstat = dots;
        pval = dots;
    end
    
    %% STEP 2 : Get parcel infos for each sEEG channel from .CSV file
    
    csvfile = fullfile(init.anat_path, init.pat_names{pat_idx}, sprintf('%s.csv', init.pat_names{pat_idx}));
    csv = csv2anat2(csvfile,chanlabels);
    
    %% Test if there is an sEEG contact within current parcel
    
    ROI_chan_idx = test_chan_parcel(csv, constraints);
    
    % Table with ROI infos
    t_roi.subname = repmat({subid(1:end-length(init.task)-1)}, length(ROI_chan_idx), 1);
    t_roi.channel = chanlabels(ROI_chan_idx);
    t_roi.marsatlas = csv.marslabels(ROI_chan_idx);
    t_roi.freesurfer = csv.freelabels(ROI_chan_idx);
    t_roi.aal = csv.aallabels(ROI_chan_idx);
    t_roi.MNIx = csv.MNI(ROI_chan_idx,1);
    t_roi.MNIy = csv.MNI(ROI_chan_idx,2);
    t_roi.MNIz = csv.MNI(ROI_chan_idx,3);
    t_roi.marsatlasfull = csv.marslabelsfull(ROI_chan_idx);
    t_roi.greywhite = csv.greywhite(ROI_chan_idx);
    T = struct2table(t_roi);
    
    display(T); % Display final electrodes selected in the ROI for this patient
    Tall = [Tall; T]; % Aggregate tables
    
    if nargout > 4
        chan_idx(pat_idx) = {ROI_chan_idx}; % Retrieves electrodes number in the parcel for each patient
    end
    
    %% Pick-up BPF data and relevent regressors
    
    for bpf = 1:length(init.freq_band) % Loop over filtered frequency bands
        
        f_range_name = sprintf('f%df%d', min(init.freq_band{bpf}), max(init.freq_band{bpf})); % Ex : f50f150
        
        for smooth = 1:length(init.smoothing) % Loop over smoothings
            
            smoothing = sprintf('sm%d',init.smoothing(smooth));
            
            for contrast = 1:length({store.(f_range_name).(smoothing).contrastname}) % Loop over contrasts
                
                if ismember(contrast, init.contrast) % Si le contraste fait partie des contrastes � regarder
                    
                    store_tmp = store.(f_range_name).(smoothing)(contrast); % "store" struct for one contrast and one frequency band
                    
                    for chan = 1:length(ROI_chan_idx) % Loop over selected channels in the parcel for the patient
                        
                        for coef = 1:min(size(store_tmp.regressor)) % Loop over contrast coefficients
                            
                            % Aggregate regression estimates across selected channels
                            dots.(f_range_name).(smoothing){coef,contrast}(:,size(dots.(f_range_name).(smoothing){coef,contrast},2)+1) = store_tmp.dots{ROI_chan_idx(chan)}(:,coef); % Tous les plots d'une parcelle pour tous les patients (timelist x nb of contact in the ROI)
                            pval.(f_range_name).(smoothing){coef,contrast}(:,size(pval.(f_range_name).(smoothing){coef,contrast},2)+1) = store_tmp.pval{ROI_chan_idx(chan)}(:,coef); % timelist x nb of contact in the ROI
                            tstat.(f_range_name).(smoothing){coef,contrast}(:,size(tstat.(f_range_name).(smoothing){coef,contrast},2)+1) = store_tmp.tstat{ROI_chan_idx(chan)}(:,coef); % timelist x nb of contact in the ROI
                            
                            if chan == length(ROI_chan_idx) % Mean of channels for each subject
                                dots.meanSubj.(f_range_name).(smoothing){coef,contrast}(:,length(unique(Tall.subname))) = mean(cell2mat(cellfun(@(v)v(:,coef), store_tmp.dots(ROI_chan_idx), 'UniformOutput', false)),2); % Moyenne de tous les plots d'une parcelle pour tous les patients (timelist x nb of subject with an electrode in the parcel)
                                pval.meanSubj.(f_range_name).(smoothing){coef,contrast}(:,length(unique(Tall.subname))) = mean(cell2mat(cellfun(@(v)v(:,coef), store_tmp.pval(ROI_chan_idx), 'UniformOutput', false)),2); % timelist x nb of subject with an electrode in the parcel
                                tstat.meanSubj.(f_range_name).(smoothing){coef,contrast}(:,length(unique(Tall.subname))) = mean(cell2mat(cellfun(@(v)v(:,coef), store_tmp.tstat(ROI_chan_idx), 'UniformOutput', false)),2); % timelist x nb of subject with an electrode in the parcel
                            end
                            
                        end % End of the loop over contrast coefficients
                    end % End of the loop over selected channels
                end % End of the 'if ismember(contrast, init.contrast)'
            end % End of the loop across contrasts
        end % End of the loop over smoothings
    end % End of the loop over filtered frequency bands
end % End of the loop across subjects

end