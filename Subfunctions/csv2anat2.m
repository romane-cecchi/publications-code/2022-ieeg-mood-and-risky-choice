% Read a .CSV file to find MNI, MarsAtlas and FreeSurfer labels of each
% channel in chanlabels cell array
%
% INPUT :
%   - csvfilename = full path of the csv filename
%   - chanlabels = cell array of channel labels we want informations
%                  (for instance : all channels from EEG data)
%
% OUTPUT :
%   - csv.chanlabels = Channel labels
%   - csv.MNI = MNI coordinates in n*3 matrix
%   - csv.marslabels = MarsAtlas labels in cell array
%   - csv.freelabels = FreeSurfer labels in cell array
%   - csv.aallabels = AAL labels in cell array
%   - csv.marslabelsfull = Full MarsAtlas labels in cell array
%   - csv.greywhite = Grey/White labels in cell array
%
%-------------------------------------------------------------------------%
% Romane Cecchi, 2019

function csv = csv2anat2(csvfile, chanlabels)

opts = detectImportOptions(csvfile);
csv_data = readtable(csvfile, opts); % Open .CSV file in table

% Find electrodes names in .CSV
col = strcmp(csv_data.Properties.VariableNames,'contact');
contact = table2cell(csv_data(:,col));
contact = regexprep(contact,'-',''); % Remove hyphen ('-') if any

% Find MNI coordinates in .CSV
col = strcmp(csv_data.Properties.VariableNames,'MNI');
mni_all = table2cell(csv_data(:,col));

% Find MarsAtlas labels in .CSV
col = strcmp(csv_data.Properties.VariableNames,'MarsAtlas');
mars_all = table2cell(csv_data(:,col));

% Find FreeSurfer labels .CSV
col = strcmp(csv_data.Properties.VariableNames,'Freesurfer');
free_all = table2cell(csv_data(:,col));

% Find AAL labels .CSV
col = strcmp(csv_data.Properties.VariableNames,'AAL');
aal_all = table2cell(csv_data(:,col));

% Find MarsAtlas full labels in .CSV
col = strcmp(csv_data.Properties.VariableNames,'MarsAtlasFull');
marsfull_all = table2cell(csv_data(:,col));

% Find grey/white labels in .CSV
col = strcmp(csv_data.Properties.VariableNames,'GreyWhite');
greywhite_all = table2cell(csv_data(:,col));

% Find information specific to the channels in EEG data
for i = 1:length(chanlabels) % Loop over channels
    
    chanidx = find(strcmp(sort(chanlabels{i}), cellfun(@sort,contact,'UniformOutput',false))); % Compare sorted string
    csv.chanlabels(i,1) = chanlabels(i);
    csv.MNI(i,:) = str2num(cell2mat(mni_all(chanidx)));
    csv.marslabels(i,1) = mars_all(chanidx);
    csv.freelabels(i,1) = free_all(chanidx);
    csv.aallabels(i,1) = aal_all(chanidx);
    csv.marslabelsfull(i,1) = marsfull_all(chanidx);
    csv.greywhite(i,1) = greywhite_all(chanidx);
    
end

end