% Compute threshold value for a contrast and a parcel
%
% INPUTS :
%   - init = Structure containing the following fields :
%       - init.pat_names = Field of patient names
%       - init.timewin = Time window in which statistical analyses are performed
%       - init.random_nb = Number of permutation for each parcel
%   - chan_idx = Cell containing electrodes number in the parcel for each patient
%   - perm_tests_all = Permutation tests of all subjects
%   - f_range_name = Name of the frequency band
%   - smoothing = Name of the smoothing
%   - timelist = Time list of the contrast
%   - permstat = Output structure
%
% OUTPUT :
%   - permstat = Structure containing the following fields :
%       - permstat.roi = Correction at the parcel level
%
% ------------------------------------------------------------------------%
% Romane Cecchi, 2020

function permstat = cluster_correction_threshold(init, chan_idx, perm_tests_all, f_range_name, smoothing, timelist, permstat)

% Initialization

idx = 1;

%% Loop over subjects

for pat_idx = 1:length(init.pat_names)
    
    if ~isempty(chan_idx{pat_idx}) % If the subject has a contact in the parcel
        
        perm_tests = perm_tests_all(pat_idx);
        
        for prdct = 1:unique(cellfun(@(a) size(a,2), perm_tests.(f_range_name).(smoothing).dots(1,chan_idx{pat_idx}))) % Loop over predictors
            
            tmpcell = cellfun(@(a) a(:,prdct,:), perm_tests.(f_range_name).(smoothing).dots(1,chan_idx{pat_idx}), 'UniformOutput', 0);
            permat.dots{prdct}(:, :, idx:idx + length(chan_idx{pat_idx}) - 1) = permute([tmpcell{:}],[1 3 2]); % Time x Permutations x Channel ; All beta for one contrast and one parcel
            
        end % End of the loop over predictors
        
        idx = idx + length(chan_idx{pat_idx});
        
    end
    
end % End of the loop over subjects

%% Randomization loop

if isempty(permstat.roi)
    
    % Time window in which statistical analyses are performed
    tw_idx = timelist >= init.timewin(1) & timelist <= init.timewin(2);
    tw_timelist = timelist(tw_idx); % Filtered timelist
    
    permstat.roi = NaN(init.random_nb, 1, size(permat.dots, 2)); % Initialization
    
    for prdct = 1:size(permat.dots, 2) % Loop over predictors
        for rd = 1:init.random_nb
            
            rdots = NaN(size(permat.dots{prdct},3), length(tw_timelist)); % Initialization
            for chan = 1:size(permat.dots{prdct},3) % Loop over channels
                ichan = randperm(size(permat.dots{prdct},2), 1); % We take a random permutation for each contact
                rdots(chan,:) = permat.dots{prdct}(tw_idx, ichan, chan); % Channel x Time
            end
            
            [h,~,~,stats] = ttest(rdots);
            L = spm_bwlabel(h); %% Find clusters
            
            % Find the biggest cluster
            stats_summary_tmp = NaN(max(L),1); % Initialization
            for k = 1:max(L) % Analyse all clusters (numbered in L)
                stats_summary_tmp(k,:) = sum(stats.tstat(L==k)); % Somme des t-values du cluster
            end
            
            % Fill the final matrix
            if isempty(stats_summary_tmp) % There is no cluster
                permstat.roi(rd, 1, prdct) = abs(stats.tstat(abs(stats.tstat) == max(abs(stats.tstat)))); % Plus grande t-value (valeur absolue)
            else
                permstat.roi(rd, 1, prdct) = abs(stats_summary_tmp(abs(stats_summary_tmp) == max(abs(stats_summary_tmp)))); % Somme des t-values du plus gros cluster (valeur absolue)
            end
            
        end % End of the loop over permutations
    end % End of the loop over predictors
    
end % End of 'if the field 'roi' doesn't exist'

end