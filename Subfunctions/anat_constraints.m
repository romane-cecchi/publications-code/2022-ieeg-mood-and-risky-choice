% Specify all the constraints that will be used to define a given ROI
%
% INPUT :
%   - Name of a parcel in char array
%
% OUTPUS :
%   - constraints.MarsAtlas_name = names of the ROI in MarsAtlas
%   - constraints.FreeSurfer_name = names of the ROI in FreeSurfer
%   - constraints.MNI_lim = struct of X, Y and Z MNI constraints (X limits are ABSOLUTE, Y and Z are RELATIVE)
%
%-------------------------------------------------------------------------%
% Romane Cecchi, 2018

function constraints = anat_constraints(parcel_name)

% Initialisation
constraints.MarsAtlas_name = {};
constraints.FreeSurfer_name = {};
constraints.AAL_name = {};
constraints.not_FreeSurfer_name = {};
constraints.not_AAL_name = {};
constraints.MNI_lim.xlim_abs_max = []; % X limits are in ABSOLUTE value
constraints.MNI_lim.xlim_abs_min = []; % X limits are in ABSOLUTE value
constraints.MNI_lim.ylim_ant = [];
constraints.MNI_lim.ylim_post = [];
constraints.MNI_lim.zlim_sup = []; % Dorsal
constraints.MNI_lim.zlim_inf = []; % Ventral

if ismember(parcel_name,'PFCvm','rows') % vmPFC
    
    % Use MarsAtlas
    constraints.MarsAtlas_name = {['R_' parcel_name],['L_' parcel_name],'PFCvm'};
    constraints.MNI_lim.xlim_abs_max = 12; % Sites more lateral will not be included in the ROI (e
    constraints.MNI_lim.zlim_sup = 10; % Sites more dorsal will not be included in the ROI
    
elseif ismember(parcel_name,'aINS','rows') % Anterior insula
    
    constraints.MNI_lim.ylim_post = 5; % Sites more posterior will not be included in the ROI
    constraints.FreeSurfer_name = {'ctx_lh_G_insular_short','ctx_rh_G_insular_short',...
        'ctx_lh_S_circular_insula_ant','ctx_rh_S_circular_insula_ant',...
        'ctx_lh_S_circular_insula_sup','ctx_rh_S_circular_insula_sup',...
        'ctx_lh_S_circular_insula_inf','ctx_rh_S_circular_insula_inf',...
        'aINS_sup','aINS_inf','aINS'};
    
elseif ismember(parcel_name,'pINS','rows') % Posterior insula
    
    constraints.MNI_lim.ylim_ant = 5; % Sites more anterior will not be included in the ROI
    constraints.FreeSurfer_name = {'ctx_rh_G_Ins_lg_and_S_cent_ins','ctx_lh_G_Ins_lg_and_S_cent_ins',...
        'ctx_lh_S_circular_insula_sup','ctx_rh_S_circular_insula_sup',...
        'ctx_lh_S_circular_insula_inf','ctx_rh_S_circular_insula_inf'};
    
elseif ismember(parcel_name,'aINS_vent','rows') % Ventral anterior insula
    
    constraints.MNI_lim.ylim_post = 5; % Sites more posterior will not be included in the ROI
    constraints.FreeSurfer_name = {'ctx_lh_G_insular_short','ctx_rh_G_insular_short',...
        'ctx_lh_S_circular_insula_ant','ctx_rh_S_circular_insula_ant',...
        'ctx_lh_S_circular_insula_inf','ctx_rh_S_circular_insula_inf','aINS_inf'};
    
elseif ismember(parcel_name,'aINS_dors','rows') % Dorsal anterior insula
    
    constraints.MNI_lim.ylim_post = 5; % Sites more posterior will not be included in the ROI
    constraints.FreeSurfer_name = {'ctx_lh_S_circular_insula_sup','ctx_rh_S_circular_insula_sup','aINS_sup'};
    
elseif ismember(parcel_name,{'Caudate','Putamen','Pallidum','Amygdala'})
    
    constraints.FreeSurfer_name = {['Left-' parcel_name],['Right-' parcel_name]};
    
elseif ismember(parcel_name,'Thalamus','rows')
    
    constraints.FreeSurfer_name = {'Left-Thalamus-Proper','Right-Thalamus-Proper'};
    
elseif ismember(parcel_name,'Hippocampus','rows')
    
    constraints.FreeSurfer_name = {'ctx_lh_AnteroHippocampus','ctx_rh_AnteroHippocampus',...
        'ctx_lh_PosteroHippocampus','ctx_rh_PosteroHippocampus','Left-Hippocampus','Right-Hippocampus'};
    
elseif ismember(parcel_name,'Accumbens','rows')
    
    constraints.FreeSurfer_name = {'Left-Accumbens-area','Right-Accumbens-area'};
    
else
    
    constraints.MarsAtlas_name = {['R_' parcel_name],['L_' parcel_name]}; % To loop across all marsAtlas parcels
    
end

% Remove parcels defined with Freesurfer or AAL labels from parcels
% defined with MarsAtlas labels

not_FreeSurfer_name = {'ctx_lh_G_insular_short','ctx_rh_G_insular_short',...
    'ctx_lh_S_circular_insula_ant','ctx_rh_S_circular_insula_ant',...
    'ctx_lh_S_circular_insula_sup','ctx_rh_S_circular_insula_sup',...
    'ctx_lh_S_circular_insula_inf','ctx_rh_S_circular_insula_inf',...
    'ctx_rh_G_Ins_lg_and_S_cent_ins','ctx_lh_G_Ins_lg_and_S_cent_ins',...
    'aINS_sup','aINS_inf',...
    'Left-Caudate','Right-Caudate','Left-Putamen','Right-Putamen',...
    'Left-Pallidum','Right-Pallidum','Left-Amygdala','Right-Amygdala',...
    'Left-Thalamus-Proper','Right-Thalamus-Proper',...
    'ctx_lh_AnteroHippocampus','ctx_rh_AnteroHippocampus',...
    'ctx_lh_PosteroHippocampus','ctx_rh_PosteroHippocampus','Left-Hippocampus','Right-Hippocampus',...
    'Left-Accumbens-area','Right-Accumbens-area'};

not_AAL_name = {};

if ~isempty(constraints.MarsAtlas_name)
    
    constraints.not_FreeSurfer_name = not_FreeSurfer_name;
    constraints.not_AAL_name = not_AAL_name;
    
elseif ~isempty(constraints.FreeSurfer_name)
    
    constraints.not_AAL_name = not_AAL_name;
    
elseif ~isempty(constraints.AAL_name)
    
    constraints.not_FreeSurfer_name = not_FreeSurfer_name;
    
end

end
