% Test if there is an sEEG contact within a parcel of interest
%
% INPUT :
%   - csv = structure containing csv info of one subject, obtained from "csv2anat2" function
%   - constraints = contraints that will be used to define the parcel of interest, obtained from "anat_constraints" function
%
% OUTPUT :
%   - ROI_chan_idx = Indexes of electrodes within the parcel of interest
%
%-------------------------------------------------------------------------%
% Romane Cecchi, 2019

function ROI_chan_idx = test_chan_parcel(csv, constraints)

ROI_chan_idx = [];

% Find contacts of the ROI using MarsAtlas labels
if ~isempty(constraints.MarsAtlas_name)
    for roi_idx = 1:length(constraints.MarsAtlas_name)
        ROI_chan_idx = [ROI_chan_idx ; find(strcmp(csv.marslabels,constraints.MarsAtlas_name{roi_idx}))];
        
        ROI_chan_idx = ROI_chan_idx(~ismember(csv.freelabels(ROI_chan_idx),constraints.not_FreeSurfer_name));
        ROI_chan_idx = ROI_chan_idx(~ismember(csv.aallabels(ROI_chan_idx), constraints.not_AAL_name));
    end
end

% Find contacts of the ROI using FreeSurfer labels
if ~isempty(constraints.FreeSurfer_name)
    for roi_idx = 1:length(constraints.FreeSurfer_name)
        ROI_chan_idx = [ROI_chan_idx ; find(strcmp(csv.freelabels, constraints.FreeSurfer_name{roi_idx}))];
        ROI_chan_idx = ROI_chan_idx(~ismember(csv.aallabels(ROI_chan_idx), constraints.not_AAL_name));
    end
end

% Find contacts of the ROI using AAL labels
if ~isempty(constraints.AAL_name)
    for roi_idx = 1:length(constraints.AAL_name)
        ROI_chan_idx = [ROI_chan_idx ; find(strcmp(csv.aallabels, constraints.AAL_name{roi_idx}))];
        ROI_chan_idx = ROI_chan_idx(~ismember(csv.freelabels(ROI_chan_idx), constraints.not_FreeSurfer_name));
    end
end

%% Delete White Matter channels

notmars = find(strcmp(csv.aallabels(ROI_chan_idx),'not in a mars atlas parcel'));
notfree = find(strcmp(csv.freelabels(ROI_chan_idx),'not in a freesurfer parcel'));
notaal = find(strcmp(csv.aallabels(ROI_chan_idx),'not in a AAL parcel'));

if ~isempty(intersect(notfree,notaal)) % Is neither a FreeSurfer parcel nor an AAL parcel
    ROI_chan_idx(intersect(notfree,notaal)) = [];
elseif ~isempty(intersect(notmars,notaal)) % Is neither a MarsAtlas parcel nor an AAL parcel
    ROI_chan_idx(intersect(notmars,notaal)) = [];
end

%% Identify channels as a function of the constraints

for roi_c = 1:length(ROI_chan_idx) % Loop over selected electrodes
    
    if  strcmp(csv.marslabels(ROI_chan_idx(roi_c)),'PFCvm') || ismember(csv.freelabels(ROI_chan_idx(roi_c)),{'aINS_sup','aINS_inf','aINS'})
        % Don't apply the constraints if the contact has been manually corrected
        % Corrections :
        %   - PFCvm
        %   - aINS_sup
        %   - aINS_inf
        %   - aINS
        
    else
        
        tmp_elec_pos = csv.MNI(ROI_chan_idx(roi_c),:);
        
        if ~isempty(constraints.MNI_lim.xlim_abs_max)
            if abs(tmp_elec_pos(1)) > constraints.MNI_lim.xlim_abs_max % If abs(X) > xlim_abs_max
                ROI_chan_idx(roi_c) = NaN; % The electrode is deselected for this parcel
            end
        end
        
        if ~isempty(constraints.MNI_lim.xlim_abs_min)
            if abs(tmp_elec_pos(1)) < constraints.MNI_lim.xlim_abs_min % If abs(X) < xlim_abs_min
                ROI_chan_idx(roi_c) = NaN; % The electrode is deselected for this parcel
            end
        end
        
        if ~isempty(constraints.MNI_lim.ylim_ant)
            if tmp_elec_pos(2) > constraints.MNI_lim.ylim_ant % If Y > ylim_ant
                ROI_chan_idx(roi_c) = NaN; % The electrode is deselected for this parcel
            end
        end
        
        if ~isempty(constraints.MNI_lim.ylim_post)
            if tmp_elec_pos(2) < constraints.MNI_lim.ylim_post % If Y < ylim_post
                ROI_chan_idx(roi_c) = NaN; % The electrode is deselected for this parcel
            end
        end
        
        if ~isempty(constraints.MNI_lim.zlim_sup)
            if tmp_elec_pos(3) > constraints.MNI_lim.zlim_sup % If Z > zlim_sup
                ROI_chan_idx(roi_c) = NaN; % The electrode is deselected for this parcel
            end
        end
        
        if ~isempty(constraints.MNI_lim.zlim_inf)
            if tmp_elec_pos(3) < constraints.MNI_lim.zlim_inf % If Z < zlim_inf
                ROI_chan_idx(roi_c) = NaN; % The electrode is deselected for this parcel
            end
        end
    end
end

ROI_chan_idx = ROI_chan_idx(~isnan(ROI_chan_idx)); % Delete NaN values

end