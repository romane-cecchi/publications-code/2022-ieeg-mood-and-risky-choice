% Band-pass filter computations from SPM files  for the paper "Intracerebral
% mechanisms explaining the impact of incidental feedback on mood state and
% risky choice"
%
% The SPM12 software is required to run this code and can be downloaded at the following address :
% https://www.fil.ion.ucl.ac.uk/spm/software/download/
%
% INPUTS :
%   - init = Structure containing the following fields :
%       - init.pat_names = Field of patient identifiers of the form XXX_201X_XXXx
%       - init.path = Database path
%       - init.freq_band (optional) = Frequency bands to filter (e.g. {50:10:150})
%       - init.comp_freq (optional) = Frequency on which bpf computation is performed
%       - init.ds_freq (optional) = Down-sampled frequency
%       - init.smoothing (optional) = Smoothing
%
% OUTPUT :
%   - One SPM file per subject, filtered frequency band, downsampling frequency and smoothing
%
% STEPS :
% (1) Band-pass filter on data :
%   - Session by session
%   - From SPM files in bipolar montage, NOT downsampled and NOT epoched
%
% (2) Epoching of new filtered files
% (3) Concatenate sessions
% (4) Delete intermediate files
%
%-------------------------------------------------------------------------%
% Romane Cecchi, 2022

function a_BPF_computations_DECID(init)

clear;
close all;

% CHOOSE FREQUENCY BANDS TO FILTER

if ~isfield(init, 'freq_band')
    init.freq_band = {50:10:150}; % {50:10:150, 4:1:8, 8:1:13, 13:5:33, 33:4:49}
end

if ~isfield(init, 'comp_freq')
    init.comp_freq = 512; % Frequency on which bpf computation is performed
end

if ~isfield(init, 'ds_freq')
    init.ds_freq = 32; % Down-sampled frequency
end

if ~isfield(init, 'smoothing')
    init.smoothing = 250; % Smoothings
end

%-------------------------------------------------------------------------%

init.max_sess = 4;

% Epoching info
init.TimeWin = [-6000 6000]; % Epoched time window
init.epoch_events = [50,51,52,53,61,62,70,71,72,73,75,100];

for pat_idx = 1:length(init.pat_names) % Boucle � travers les sujets
    
    clearvars -except init pat_idx
    subjName = init.pat_names{pat_idx};
    
    % Patient file path
    patfile = fullfile(init.path, subjName, sprintf('%s_%s', subjName, 'DECID'));
    
    for sess = 1:init.max_sess % Loop across sessions
        
        % SPM file path (bipolar montage, not epoched, not downsampled)
        spmfile = fullfile(patfile, 'Analyses', 'BPF', 'RAW', sprintf('%s_%s%d_BPF.mat', subjName, 'DECID', sess)); % Full path of SPM file
        
        if exist(spmfile, 'file') % Si la session existe
            
            % Load data
            D = spm_eeg_load(spmfile); % SPM struct
            
            % Down-sample to 512 Hz if this is a 1024 Hz file (or more) : (create d.mat)
            if D.fsample > init.comp_freq
                S = [];
                S.D = D.fullfile; % Not down-sampled and not epoched file
                S.fsample_new = init.comp_freq;
                D = spm_eeg_downsample(S);
            end
            
            %% Loop over frequency band
            
            fileout = fullfile(patfile, 'Analyses', 'BPF', sprintf('%s_%s%d', subjName, 'DECID', sess)); % Full path of the output file WITHOUT extension
            
            for bpf = 1:length(init.freq_band)
                
                f_range = init.freq_band{bpf}; % Frequency range
                smoothing = init.smoothing; % Smoothing windows in ms
                newfiles = spm2env(D,f_range,fileout,smoothing); % Band-pass filtering
                
                % Epoch new SPM files
                for sm = 1:length(smoothing) % Loop over smoothings
                    
                    newfilename = newfiles{sm};
                    
                    % Down-sample at a defined frequency
                    S = [];
                    S.D = newfilename; % BPF file
                    S.fsample_new = init.ds_freq;
                    E = spm_eeg_downsample(S);
                    
                    % Change file name
                    newfilename = sprintf('%s_f%df%d_sf%d_sm%d', fileout, min(f_range), max(f_range), E.fsample, smoothing(sm));
                    Dnew = copy(E,newfilename);
                    save(Dnew);
                    
                    S = [];
                    S.D = newfilename; % Nom du fichier filtr� et sous-�chantillonn�
                    S.bc = 0;
                    S.timewin = init.TimeWin;
                    
                    for i = 1:length(init.epoch_events)
                        S.trialdef(i).conditionlabel = num2str(init.epoch_events(i));
                        S.trialdef(i).eventtype = num2str(init.epoch_events(i));
                        S.trialdef(i).eventvalue = init.epoch_events(i);
                    end
                    
                    S.fsample = E.fsample;
                    S.timeonset = 0;
                    S.inputformat = [];
                    S.reviewtrials = 0;
                    S.save = 1;
                    S.epochinfo.padding = 0;
                    
                    F = spm_eeg_epochs(S); % channels x temps x trial (pr�fixe e)
                    
                    epochfiles{sess,bpf,sm} = F.fullfile;
                    
                end % End of the loop over smoothings
                
            end % End of the loop over filtered frequency band
        end
        
    end % Fin de la boucle � travers les sessions
    
    %% CONCATENATION OF ALL SESSIONS
    
    for bpf = 1:length(init.freq_band) % Loop over frequency band
        for sm = 1:length(smoothing) % Loop over smoothings
            
            clear concatfiles;
            concatfiles(:,:) = char(epochfiles(~cellfun(@isempty, epochfiles(:,bpf,sm)),bpf,sm)); % Don't work if filenames are not the same length...
            
            S = [];
            S.D = concatfiles;
            S.recode = 'same';
            S.fileOut = sprintf('%s_%s%s', subjName, 'DECID', concatfiles(1,regexp(concatfiles(1,:),'_f\d'):end-4));
            S.prefix = '';
            D = spm_eeg_merge_RC(S);
            
        end % End of the loop over smoothings
    end % End of the loop over filtered frequency band
    
    close all;
    
    %% DELETE ALL INTERMEDIATE FILES
    
    files = dir(D.path);
    fold = files.folder;
    filenames = {files.name};
    
    % Delete intermediate files for each session
    expr = sprintf('%s%s', 'DECID', '\d'); % Find all filenames containing DECIDx (x = a number from 0-9)
    delfiles = regexp(filenames(1,:), expr, 'match');
    delfiles = filenames(~cellfun(@isempty, delfiles));
    
    % Delete files
    for i = 1:length(delfiles)
        delete(fullfile(fold, delfiles{i}));
    end
    
    % Delete also downsampled files in the RAW folder
    files = dir(fullfile(fold, 'RAW'));
    fold = files.folder;
    filenames = {files.name};
    
    delfiles2 = regexp(filenames(1,:), sprintf('d%s', subjName), 'match');
    delfiles2 = filenames(~cellfun(@isempty, delfiles2));
    
    % Delete files
    for i = 1:length(delfiles2)
        delete(fullfile(fold, delfiles2{i}));
    end
    
end % Fin de la boucle � travers les sujets

end % End of the function
